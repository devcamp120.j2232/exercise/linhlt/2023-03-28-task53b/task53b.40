import s50.*;
public class Task53B40 {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Linh","Hanoi");
        Person person2 = new Person("Minh","Danang");
        Student student1 = new Student(person1.getName(), person1.getAddress(), "Java Basic", 2023, 1000000);
        Student student2 = new Student(person2.getName(), person2.getAddress(), "Crypto Basic", 2023, 2000000);
        Staff staff1 = new Staff(person1.getName(),person1.getAddress(),"Havard",25000000);
        Staff staff2 = new Staff(person2.getName(),person2.getAddress(),"NEU",15000000);
        System.out.println(person1);
        System.out.println(person2);
        System.out.println(student1);
        System.out.println(student2);
        System.out.println(staff1);
        System.out.println(staff2);
    }
}
